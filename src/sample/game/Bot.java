package sample.game;

import java.util.Random;

public class Bot extends Player {
    private static int nameCount = 0;
    private static Random rnd = new Random();

    public Bot() {
        name = "Bot #" + nameCount++;
        hitPoints = 100 + rnd.nextInt(101);
        strength = 1 + rnd.nextInt(10);
    }
    public int getDefense(){
       return 1+rnd.nextInt(4) ;
    }
    public int getAttack(){
        return 1+rnd.nextInt(4) ;
    }
}
