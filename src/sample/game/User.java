package sample.game;

public class User extends Player {
    public User(String name) {
        this.name = name;
        strength = 20;
        hitPoints = 150;
    }
}
