package sample;

import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import sample.game.Bot;
import sample.game.User;

public class Main extends Application {
    Bot bot = new Bot();
    User user = new User("Vasya Pupking");
    private Button button1, button2, button3, button4;
    private RadioButton radioButton1, radioButton2, radioButton3, radioButton4;
    private RadioButton radioButtonA, radioButtonB, radioButtonC, radioButtonD;
    int buttonClickCounter;
    private TextArea textArea;
    private ToggleGroup leftGroup;
    private ToggleGroup rightGroup;


    @Override
    public void start(Stage primaryStage) throws Exception {
        System.out.println("start");
        GridPane root = new GridPane();
        root.setGridLinesVisible(false);
        ColumnConstraints columnConstraints = new ColumnConstraints();
        columnConstraints.setPercentWidth(50);
        root.getColumnConstraints().add(0, columnConstraints);
        root.getColumnConstraints().add(1, columnConstraints);
        RowConstraints rowConstraints = new RowConstraints();
        rowConstraints.setPercentHeight(10);
        root.getRowConstraints().add(0, rowConstraints);
        RowConstraints rowConstraints1 = new RowConstraints();
        rowConstraints1.setPercentHeight(55);
        root.getRowConstraints().add(1, rowConstraints1);
        RowConstraints rowConstraints3 = new RowConstraints();
        rowConstraints3.setPercentHeight(10);
        root.getRowConstraints().add(2, rowConstraints3);
        RowConstraints rowConstraints2 = new RowConstraints();
        rowConstraints2.setPercentHeight(25);
        root.getRowConstraints().add(3, rowConstraints2);

        Label labelLeft = new Label("Attack");
        Label labelRight = new Label("Defense");
        button1 = new Button("Click Me");
        button1.setOnAction(event -> {
            nextMove();
        });
        leftGroup = new ToggleGroup();
        radioButton1 = new RadioButton("Head");
        radioButton1.setUserData(1);
        radioButton1.setToggleGroup(leftGroup);
        radioButton1.setSelected(true);
        radioButton2 = new RadioButton("Torso");
        radioButton2.setUserData(2);
        radioButton2.setToggleGroup(leftGroup);
        radioButton3 = new RadioButton("Hands");
        radioButton3.setUserData(3);
        radioButton3.setToggleGroup(leftGroup);
        radioButton4 = new RadioButton("Legs");
        radioButton4.setUserData(4);
        radioButton4.setToggleGroup(leftGroup);
        rightGroup = new ToggleGroup();
        radioButtonA = new RadioButton("Head,Torso");
        radioButtonA.setUserData(1);
        radioButtonA.setToggleGroup(rightGroup);
        radioButtonA.setSelected(true);
        radioButtonB = new RadioButton("Torso,Hands");
        radioButtonB.setUserData(2);
        radioButtonB.setToggleGroup(rightGroup);
        radioButtonC = new RadioButton("Hands,Legs");
        radioButtonC.setUserData(3);
        radioButtonC.setToggleGroup(rightGroup);
        radioButtonD = new RadioButton("Legs,Head");
        radioButtonD.setUserData(4);
        radioButtonD.setToggleGroup(rightGroup);
        //textArea = new TextArea("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
        textArea = new TextArea();
        root.add(textArea, 0, 3);
        root.add(labelLeft, 0, 0);
        root.add(labelRight, 1, 0);
        root.add(button1, 0, 2);
        GridPane.setValignment(textArea, VPos.CENTER);
        GridPane.setHalignment(textArea, HPos.CENTER);
        GridPane.setColumnSpan(textArea, 2);
        textArea.setEditable(false);
        textArea.setWrapText(true);
        VBox vBox = new VBox(radioButton1, radioButton2, radioButton3, radioButton4);
        FlowPane flowVbox = new FlowPane();
        flowVbox.getChildren().add(vBox);
        vBox.setAlignment(Pos.CENTER_LEFT);
        FlowPane flowVbox2 = new FlowPane();
        VBox vBoxRight = new VBox(radioButtonA, radioButtonB, radioButtonC, radioButtonD);
        vBox.setSpacing(15);
        vBoxRight.setSpacing(15);
        flowVbox2.getChildren().add(vBoxRight);
        vBoxRight.setAlignment(Pos.CENTER_LEFT);
        root.add(flowVbox, 0, 1);
        root.add(flowVbox2, 1, 1);
        flowVbox.setAlignment(Pos.CENTER);
        flowVbox2.setAlignment(Pos.CENTER);
        GridPane.setValignment(flowVbox, VPos.CENTER);
        GridPane.setHalignment(flowVbox, HPos.CENTER);
        GridPane.setValignment(flowVbox2, VPos.CENTER);
        GridPane.setHalignment(flowVbox2, HPos.CENTER);
        GridPane.setValignment(labelLeft, VPos.CENTER);
        GridPane.setHalignment(labelLeft, HPos.CENTER);
        GridPane.setValignment(labelRight, VPos.CENTER);
        GridPane.setHalignment(labelRight, HPos.CENTER);
        GridPane.setColumnSpan(button1, 2);
        GridPane.setValignment(button1, VPos.CENTER);
        GridPane.setHalignment(button1, HPos.CENTER);
        primaryStage.setTitle("Mortal Combat");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();
    }

    @Override
    public void init() throws Exception {
        super.init();
        System.out.println("init");
    }

    public static void main(String[] args) {
                System.out.println("main");
        launch(args);
    }
private void finishGame(String message){
    Alert alert = new Alert(Alert.AlertType.INFORMATION);
    alert.setTitle("Information Dialog");
    alert.setHeaderText("Look, an Information Dialog");
    alert.setContentText(message);

    alert.showAndWait();
}
    private void nextMove() {
        int playerAttack = (int) leftGroup.getSelectedToggle().getUserData();
        int playerDefense = (int) rightGroup.getSelectedToggle().getUserData();
        int botAttack = bot.getAttack();
        int botDefense = bot.getAttack();
        System.out.println(playerAttack);
        System.out.println(playerDefense);
        System.out.println("Next stage");
        if (botAttack != playerDefense && botAttack != playerDefense + 1) {
            if (botAttack != 1 && playerDefense != 4) {
                user.setHitPoints(user.getHitPoints() - bot.getStrength());
            }
        }
        if (playerAttack != botDefense && playerAttack != botDefense + 1) {
            if (playerAttack != 1 || botDefense != 4) {
                bot.setHitPoints(bot.getHitPoints() - user.getStrength());
            }
        }
        textArea.appendText("User hit points " + user.getHitPoints() +
                "Bot's hit points " + bot.getHitPoints() + "\n");
        if (user.getHitPoints() <= 0 && bot.getHitPoints() <= 0) {
            textArea.appendText("Draw!!!");
            finishGame("Draw");
            System.exit(0);
        } else if (user.getHitPoints() <= 0) {
            textArea.appendText("You lost!");
            finishGame("You Lost!");
            System.exit(0);
        } else if (bot.getHitPoints() <= 0) {
            finishGame("You Win!");
            textArea.appendText("You win!");
            System.exit(0);
        }
    }

}

